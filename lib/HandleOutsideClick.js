'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HandleOutsideClick = function (_Component) {
  _inherits(HandleOutsideClick, _Component);

  function HandleOutsideClick(props) {
    _classCallCheck(this, HandleOutsideClick);

    var _this = _possibleConstructorReturn(this, (HandleOutsideClick.__proto__ || Object.getPrototypeOf(HandleOutsideClick)).call(this, props));

    _this.onOutsideClick = _this.onOutsideClick.bind(_this);
    return _this;
  }

  _createClass(HandleOutsideClick, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      document.addEventListener('click', this.onOutsideClick);

      if ('ontouchstart' in document.documentElement) {
        document.body.style.cursor = 'pointer';
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      document.removeEventListener('click', this.onOutsideClick);
    }
  }, {
    key: 'onOutsideClick',
    value: function onOutsideClick(e) {
      var insideClick = this.wrapperElem.contains(e.target);
      if (!insideClick && this.props.onOutsideClick) this.props.onOutsideClick(e);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'span',
        { ref: function ref(elem) {
            _this2.wrapperElem = elem;
          } },
        this.props.children
      );
    }
  }]);

  return HandleOutsideClick;
}(_react.Component);

HandleOutsideClick.propTypes = {
  children: _propTypes2.default.node.isRequired,
  onOutsideClick: _propTypes2.default.func
};

HandleOutsideClick.defaultProps = {
  onOutsideClick: function onOutsideClick() {}
};

exports.default = HandleOutsideClick;