// Configure JSDOM and set global variables to simulate a browser environment for tests
import jsdom from 'jsdom'

const exposedProperties = ['window', 'navigator', 'document']

global.document = jsdom.jsdom('')
// Mock window
global.window = document.defaultView

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property)
    global[property] = document.defaultView[property]
  }
})

// Simulate window resize event
const resizeEvent = document.createEvent('Event')
resizeEvent.initEvent('resize', true, true)

global.window.resizeTo = (width, height) => {
  global.window.innerWidth = width || global.window.innerWidth
  global.window.innerHeight = width || global.window.innerHeight
  global.window.dispatchEvent(resizeEvent)
}

global.navigator = {
  userAgent: 'node.js',
}
