React Priority Nav
---------------

A React alternative to the priority nav


Getting Started ⚡️
---------------

1. Install:

  ```
  npm install --save https://bitbucket.org/simplesteam/react-priority-nav.git
  ```

2. Use:

  ```
  import PriorityNav from 'react-priority-nav'
  ```

  ```
  <PriorityNav>
    <ul>
      <li>
        <a href="#">Home</a>
      </li>
      <li>
        <a href="#">About</a>
      </li>
      <li>
        <a href="#">Contact</a>
      </li>
    </ul>
  </PriorityNav>
  ```
