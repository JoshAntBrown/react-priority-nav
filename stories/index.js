import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import { PriorityNav, HandleOutsideClick } from '../src/index' // eslint-disable-line

import '../priority-nav.css'

class Demo extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showing: false,
    }
  }

  render() {
    return (
      <div>
        <PriorityNav>
          <ul>
            <li>
              <a href="#home">Home</a>
            </li>
            <li>
              <a href="#about">About</a>
            </li>
            <li>
              <a href="#contact">Contact</a>
            </li>
            <li>
              <a href="#this">This</a>
            </li>
            <li>
              <a href="#is">is</a>
            </li>
            <li>
              <a href="#my">my</a>
            </li>
            <li>
              <a href="#react">React</a>
            </li>
            <li>
              <a href="#priority">Priority</a>
            </li>
            <li>
              <a href="#navigation">Navigation</a>
            </li>
            {
              (this.state.showing) && (
                <li>
                  <a href="#item">Optional Item</a>
                </li>
              )
            }
          </ul>
        </PriorityNav>

        <button
          onClick={() => {
            this.setState({
              showing: !this.state.showing,
            })
          }}
        >
          Toggle Item
        </button>
      </div>
    )
  }
}

storiesOf('Demo', module)
  .add('PriorityNav', () => <Demo />)
